<%

/**
 * Service object for service items and their properties
 */
class Service {
	private String serviceCode = "";
	private String description = "";
	private boolean metadata = false;
	private String type = "";

	public boolean valid = false;

	// Method to set object attributes based on serviceCode
	public Service setService(String serviceCode) {
		this.serviceCode = serviceCode;
		switch(serviceCode) {
			case "BLOCKED_DRAIN_GULLY":
				this.description = "Blocked Drain";
				this.metadata = true;
				this.type = "realtime";
				this.valid = true;
				break;
			case "CAR_PARK_CLEANSING":
				this.description = "Car Park Cleansing";
				this.metadata = true;
				this.type = "realtime";
				this.valid = true;
				break;
			case "DEAD_ANIMAL":
				this.description = "Dead animals";
				this.metadata = true;
				this.type = "realtime";
				this.valid = true;
				break;
			case "FLY_TIPPING":
				this.description = "Fly Tipping";
				this.metadata = true;
				this.type = "realtime";
				this.valid = true;
				break;
			default:
				// 404
				this.valid = false;
				break;
		}
		return this;
	}

	// Constructor method, call this.setService() on new object instance
	public Service(String serviceCode) {
		this.setService(serviceCode);
	}

	// return a JSON-formatted string representing this object
	public String getJSON() {
		String json = "{";
		json+= "\"serviceCode\": \""+this.serviceCode+"\",";
		json+= "\"description\": \""+this.description+"\",";
		json+= "\"metadata\": "+String.valueOf(this.metadata)+",";
		json+= "\"type\": \""+this.type+"\"";
		json+= "}";
		return json;
	}
}

// Array of valid service codes
String[] serviceCodes = {
	"BLOCKED_DRAIN_GULLY",
	"CAR_PARK_CLEANSING",
	"DEAD_ANIMAL",
	"FLY_TIPPING"
};

if (request.getParameter("ServiceCode")==null) {
	// Print all services
	String output = "[";
	for (int i=0; i<serviceCodes.length; i++) {
		if (output!="[") output+= ",";
		Service this_service = new Service(serviceCodes[i]);
		output+= this_service.getJSON();
	}
	output+= "]";
	response.setContentType("text/json");
	out.println(output);
} else {
	// Print specific service, or 404
	Service this_service = new Service(request.getParameter("ServiceCode"));
	if (this_service.valid) {
		response.setContentType("text/json");
		out.println(this_service.getJSON());
	} else {
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
}

%>
